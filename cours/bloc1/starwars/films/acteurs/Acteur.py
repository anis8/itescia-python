class Acteur:

    def __init__(self, nom, prenom):
        self.nom = nom
        self.prenom = prenom
        self.personnages = []

    def getNom(self):
        return self.nom

    def getPrenom(self):
        return self.prenom

    def setNom(self, nom):
        self.nom = nom

    def setPrenom(self, prenom):
        self.prenom = prenom

    def __str__(self):
        return "nom: " + self.nom + " prenom: " + self.prenom

    def addPersonnage(self, personnage):
        self.personnages.append(personnage)

    def removePersonnage(self, personnage):
        self.personnages.remove(personnage)

    def getNbPersonnages(self):
        return len(self.personnages)

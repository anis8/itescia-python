class Personnage:

    def __init__(self, nom, prenom):
        self.nom = nom
        self.prenom = prenom

    def getNom(self):
        return self.nom

    def getPrenom(self):
        return self.prenom

    def setNom(self, nom):
        self.nom = nom

    def setPrenom(self, prenom):
        self.prenom = prenom

    def __str__(self):
        return "Nom: " + self.nom + " Prénom: " + self.prenom

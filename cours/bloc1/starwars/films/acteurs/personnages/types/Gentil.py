from cours.bloc1.starwars.films.acteurs.personnages.Personnage import Personnage


class Gentil(Personnage):
    def __init__(self, nom, prenom, force):
        super().__init__(nom, prenom)
        self.force = force

    def getNom(self):
        return self.nom

    def getPrenom(self):
        return self.prenom

    def getForce(self):
        return self.force

    def setNom(self, nom):
        self.nom = nom

    def setPrenom(self, prenom):
        self.prenom = prenom

    def setForce(self, force):
        self.force = force

    def __str__(self):
        return "Nom: " + self.nom + " Prenom: " + self.prenom + " Force: " + self.force

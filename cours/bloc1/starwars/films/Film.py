class Film:

    def __init__(self, titre, anneeSortie, numeroEpisode, cout, recette):
        self.titre = titre
        self.anneeSortie = anneeSortie
        self.numeroEpisode = numeroEpisode
        self.cout = cout
        self.recette = recette
        self.acteurs = []

    def getTitre(self):
        return self.titre

    def getAnneeSortie(self):
        return self.anneeSortie

    def getNumeroEpisode(self):
        return self.numeroEpisode

    def getCout(self):
        return self.cout

    def getRecette(self):
        return self.recette

    def setTitre(self, titre):
        self.titre = titre

    def setAnneeSortie(self, anneeSortie):
        self.anneeSortie = anneeSortie

    def setNumeroEpisode(self, numeroEpisode):
        self.numeroEpisode = numeroEpisode

    def setCout(self, cout):
        self.cout = cout

    def setRecette(self, recette):
        self.recette = recette

    def __str__(self):
        return "Titre: " + self.titre + " année de sortie: " + self.anneeSortie + " numéro d'épisode: " + self.numeroEpisode + " coût: " + self.cout + " recette:" + self.recette

    def backup(self):
        return "Titre: " + self.titre + " année de sortie: " + str(self.anneeSortie) + " - " + str(self.calculBenefice()[0])

    def addActeur(self, acteur):
        self.acteurs.append(acteur)

    def addActeurs(self, acteurs):
        for i in range(0, len(acteurs)):
            self.acteurs.append(acteurs[i])

    def removeActeur(self, acteur):
        self.acteurs.remove(acteur)

    def fetchActeur(self):
        for i in range(0, len(self.acteurs)):
            print(self.acteurs[i])

    def getActeur(self, index):
        return self.acteurs[index]

    def nbActeurs(self):
        return len(self.acteurs)

    def nbPersonnages(self):
        nb = 0
        for i in range(0, len(self.acteurs)):
            for x in range(0, len(self.acteurs[i].personnages)):
                nb = nb + 1

        return nb

    def calculBenefice(self):
        if self.recette >= self.cout:
            return [self.recette - self.cout, True]
        else:
            return [self.cout - self.recette, False]

    def isBefore(self, date):
        return date > self.anneeSortie

    def tri(self):
        return self.acteurs.sort(key=lambda x: x.nom)

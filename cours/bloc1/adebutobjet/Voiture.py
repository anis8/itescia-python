import datetime


class Voiture:
    def __init__(self):
        self.modele = "Mondeo"
        self.immat = "000XX00"
        self.dateCirculation = "10-09-2000"

    def getImmat(self):
        return self.immat

    def setImmat(self, immat):
        self.immat = immat

    def setModele(self, modele):
        self.modele = modele

    def setDateCirculation(self, date):
        self.dateCirculation = date

    def getDateCirculation(self):
        return self.dateCirculation

    def getModele(self):
        return self.dateCirculation

    # 3 chiffres + 2 lettres + 2 chiffres entre 1 & 97
    def estFr(self):
        if self.immat[:3].isdigit() and not self.immat[3:5].isdigit() and self.immat[5:7].isdigit() and int(
                self.immat[5:7]) >= 1 and int(self.immat[5:7]) <= 97:
            return True
        return False

    def estDeCollection(self):
        now = datetime.datetime.now()
        if (now.year - 20) >= int(self.dateCirculation[6:11]):
            return True
        return False

    def __str__(self):
        return "Modele: " + self.modele + " immatriculation: " + self.immat + " date: " + self.dateCirculation

class Ville:

    def __init__(self, nom, nbHabitants=0):
        self.nom = nom
        self.nbHabitants = nbHabitants

    def getNom(self):
        return self.nom

    def setNom(self, nom):
        self.nom = nom

    def getNbHabitants(self):
        if self.nbHabitants == None:
            return 0
        return self.nbHabitants

    def setNbHabitants(self, nbHabitants):
        if nbHabitants < 0:
            nbHabitants = 0
        self.nbHabitants = nbHabitants

    def nbHabitantsConnu(self):
        if self.getNbHabitants() == 0:
            return False
        else:
            return True
    def __str__(self):
        return "La ville est : " + self.nom + " elle est peuplé de : " + str(self.nbHabitants) + " habitants"

    def categorie(self):
        if self.nbHabitants < 500000 and self.nbHabitants > 0:
            return 'A'
        elif self.nbHabitants >= 500000:
            return 'B'
        elif self.nbHabitants == 0:
            return '?'
